# Utxo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asset_path** | Option<**String**> | Asset path of transferred currency | [optional]
**address** | Option<**String**> |  | [optional]
**value** | Option<**String**> | Integer string in smallest unit (Satoshis) | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


