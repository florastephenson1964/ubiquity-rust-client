# Error

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_type** | Option<**String**> | HTTP error type | [optional]
**code** | Option<**i32**> | Numeric error code | [optional]
**title** | Option<**String**> | Short error description | [optional]
**status** | Option<**i32**> | HTTP status of the error | [optional]
**detail** | Option<**String**> | Long error description | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


