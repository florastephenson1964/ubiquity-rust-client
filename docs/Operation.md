# Operation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_type** | **String** |  | [default to multi_transfer]
**event** | Option<**String**> |  | [optional]
**detail** | [**crate::models::MultiTransfer**](multi_transfer.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


